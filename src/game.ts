import * as PIXI from "pixi.js";
import {CONFIG} from "./config";
import {Grid} from "./grid/grid";
import {screenH, screenW} from "./index";
import {EntityInterface} from "./entity/entity.interface";
import {HeatManager} from "./util/heat-manager";

/**
 * Main game object
 */
export class Game {

    public app: PIXI.Application;
    public grid: Grid;
    public entities: EntityInterface[] = [];
    public heatManager: HeatManager;

    /**
     * Constructor.
     */
    constructor() {
        // Create new pixi app for given screen size
        this.app = new PIXI.Application({width: screenW, height: screenH, backgroundColor: 0xFFFFFF});
        document.body.appendChild(this.app.view);
    }

    /**
     * Init the game scene
     */
    public init() {
        // Create grid and add to stage
        this.grid = new Grid();
        this.grid.registerContainers(this.app.stage);

        // Create new heat manager
        this.heatManager = new HeatManager(this);

        // Register step and draw timers
        setInterval(() => this.step(), 1000 / CONFIG.TICK_RATE);
        this.app.ticker.add((d) => this.draw(d));
    }

    /**
     * Add entity to game
     * @param entity
     */
    public addEntity(entity: EntityInterface) {
        this.entities.push(entity);
        entity.registerGraphics(this.grid.gameContainer);
        entity.cell = this.grid.getCellAt(entity.x, entity.y);
    }

    /**
     * Execute a step
     */
    public step() {
        // todo: implement
        // this.heatManager.spreadHeat();
    }

    /**
     * Execute a draw event
     * @param d
     */
    public draw(d: number) {
        // todo: implement
    }
}
