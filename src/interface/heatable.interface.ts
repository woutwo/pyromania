export interface HeatableInterface {

    /**
     * Heat transfer coefficient
     * c = W/(m2 K)
     */
    getHeatTransferCoefficient(): number;

    /**
     * Heat capacity
     * c = J/(kg K)
     */
    getHeatCapacity(): number;

    /**
     * Cell temperature (T)
     */
    getTemperature(): number;

    /**
     * Surface area (m2)
     */
    getSurfaceArea(): number;

    /**
     * Mass (kg)
     */
    getMass(): number;

    /**
     * Energy (J)
     */
    getEnergy(): number;

    /**
     * Add energy to object
     * @param energy
     */
    addEnergy(energy: number): void;
}
