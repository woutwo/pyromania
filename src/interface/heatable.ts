import {HeatableInterface} from "./heatable.interface";
import {ROOM_TEMP} from "../config";

export class Heatable implements HeatableInterface {

    protected heatCapacity: number = 1;
    protected heatTransferCoefficient = 500;
    protected temperature: number = ROOM_TEMP;
    protected surfaceArea: number = 1;
    protected mass: number = 1;

    public getHeatCapacity() {
        return this.heatCapacity;
    }

    public getHeatTransferCoefficient() {
        return this.heatTransferCoefficient;
    }

    public getTemperature() {
        return this.temperature;
    }

    public getSurfaceArea() {
        return this.surfaceArea;
    }

    public getMass(): number {
        return this.mass;
    }

    public getEnergy() {
        return this.temperature;
    }

    public addEnergy(energy: number) {
        this.temperature += energy / (this.heatCapacity * this.mass);
    }
}
