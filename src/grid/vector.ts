/**
 * Vector
 *
 * 2D vector class with utility methods
 */
export class Vector {
    public x: number;
    public y: number;

    /**
     * Construct new vector with given legs
     * @param x
     * @param y
     */
    constructor(x: number, y: number) {
        this.x = x;
        this.y = y;
    }

    /**
     * Calculate vector length
     */
    public get length(): number {
        return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2));
    }

    /**
     * Set vector length
     * @param length
     */
    public set length(length: number) {
        const angle = this.angle;
        this.x = Math.cos(angle) * length;
        this.y = Math.sin(angle) * length;
    }

    /**
     * Get vector angle
     */
    public get angle(): number {
        return Math.atan2(this.y, this.x);
    }

    /**
     * Set vector angle
     * @param angle
     */
    public set angle(angle: number) {
        const length = this.length;
        this.x = Math.cos(angle) * length;
        this.y = Math.sin(angle) * length;
    }

    /**
     * Add given vector to the vector
     * @param vector
     */
    public add(vector: Vector): Vector {
        this.x += vector.x;
        this.y += vector.y;
        return this;
    }

    /**
     * Subtract given vector of the vector
     * @param vector
     */
    public sub(vector: Vector): Vector {
        this.x -= vector.x;
        this.y -= vector.y;
        return this;
    }

    /**
     * Create copy of the vector
     */
    public copy(): Vector {
        return new Vector(this.x, this.y);
    }

    /**
     * Invert the vector
     */
    public invert(): Vector {
        this.x *= -1;
        this.y *= -1;
        return this;
    }
}
