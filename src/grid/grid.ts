import DisplayObject = PIXI.DisplayObject;
import {screenH, screenW} from "../index";
import {Cell} from "./cell";
import {CONFIG} from "../config";

/**
 * Game grid
 *
 * The grid holds the objects that are present in the game scene and takes care of the scaling and positioning
 * of the objects.
 */
export class Grid {

    public static size = CONFIG.CELL_SIZE;
    public width: number;
    public height: number;
    public gridContainer = new PIXI.Container();
    public gameContainer = new PIXI.Container();
    public cells: Cell[] = [];
    protected cellIndexes = new PIXI.Graphics();

    constructor() {
        // Calculate grid dimensions
        this.calculateGridSize();

        // Scale the container according to the cell size
        this.gridContainer.scale.x = Grid.size;
        this.gridContainer.scale.y = Grid.size;

        // Position the container in the center of the screen
        this.gridContainer.pivot.set(0.5, 0.5);
        this.gridContainer.position.x = screenW / 2;
        this.gridContainer.position.y = screenH / 2;

        // Position the container in the center of the screen
        this.cellIndexes.pivot.set(0.5, 0.5);
        this.cellIndexes.position.x = screenW / 2;
        this.cellIndexes.position.y = screenH / 2;

        this.gridContainer.addChild(this.gameContainer);

        // Create the grid cells and draw the grid lines
        this.createCells();
        this.drawCells();
    }

    /**
     * Calculate the grid size from the screen size and sprite sizes
     */
    private calculateGridSize() {
        this.width = 16; // Math.floor(screenW / Grid.size);
        this.height = 16; // Math.floor(screenH / Grid.size);

        // Make sure the field dimensions are always uneven (to make sure 0,0 is always in center)
        if (this.width % 2 === 0) {
            this.width--;
        }
        if (this.height % 2 === 0) {
            this.height--;
        }
    }

    /**
     * Add child object to the grid
     * @param child
     */
    public addChild(child: DisplayObject) {
        this.gameContainer.addChild(child);
    }

    /**
     * Create grid cells
     */
    public createCells() {
        let cell: Cell;
        for (let y = -Math.floor(this.height / 2); y < Math.ceil(this.height / 2); y++) {
            for (let x = -Math.floor(this.width / 2); x < Math.ceil(this.width / 2); x++) {
                // Create new cell
                cell = new Cell(x, y);
                this.cells.push(cell);

                // Add cell to grid container
                this.addChild(cell.graphics);
            }
        }
    }

    /**
     * Get cell at given coordinate
     * @param x
     * @param y
     */
    public getCellAt(x: number, y: number) {
        // Because of the 0,0 origin with negative coordinates we need to offset all coordinates with half the size
        if (x < -this.width / 2 || y < -this.height / 2 || x >= this.width / 2 || y >= this.height / 2) {
            return undefined;
        }
        const offset = Math.floor(this.height * this.width / 2);
        return this.cells[(y * this.width + x) + offset];
    }

    /**
     * Get cell index for given coordinate
     * @param x
     * @param y
     */
    public getCellIndex(x: number, y: number) {
        // Because of the 0,0 origin with negative coordinates we need to offset all coordinates with half the size
        return (y * this.width + x) + Math.floor(this.height * this.width / 2);
    }

    /**
     * Draw the cells
     */
    public drawCells() {
        // Clear drawn cell numbers
        this.cellIndexes.clear();
        for (const cell of this.cells) {
            // Draw cell
            cell.draw();

            // Draw cell numbers
            const text = new PIXI.Text(this.cells.indexOf(cell).toString(), {
                fontFamily: "Arial",
                fontSize: 8,
                fill: 0x000000,
            });
            text.position.x = cell.x * Grid.size - Grid.size / 2;
            text.position.y = cell.y * Grid.size - Grid.size / 2;
            this.cellIndexes.addChild(text);
        }
    }

    /**
     * Register containers to given stage
     * @param stage
     */
    public registerContainers(stage: PIXI.Container) {
        stage.addChild(this.gridContainer);
        stage.addChild(this.cellIndexes);
    }
}
