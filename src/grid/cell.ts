import {Grid} from "./grid";
import {ROOM_TEMP} from "../config";

export class Cell {

    /**
     * Sprite container
     */
    public graphics: PIXI.Graphics = new PIXI.Graphics();

    /**
     * Heat transfer coefficient
     * c = W/(m2 K)
     */
    public heatTransferCoefficient = 500;

    /**
     * Energy in cell (J)
     */
    public energy = 0;

    /**
     * Cell temperature (T)
     */
    public temperature = ROOM_TEMP;

    /**
     * Constructor
     * @param x
     * @param y
     */
    constructor(public x: number, public y: number) {
    }

    /**
     * Draw the sprite
     */
    public draw() {
        this.graphics.clear();
        this.graphics.lineStyle(1 / Grid.size, 0xAAAAAA);
        this.graphics.drawRect(this.x, this.y, 1, 1);
    }
}
