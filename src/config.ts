/**
 * Configuration file
 */
export let CONFIG = {
    TICK_RATE: 60,
    CELL_SIZE: 32,
};

export const ROOM_TEMP = 273;
