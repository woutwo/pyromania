import {EntityInterface} from "./entity.interface";
import {Cell} from "../grid/cell";

export abstract class Entity implements EntityInterface {
    public cell: Cell;
    public graphics: PIXI.Graphics = new PIXI.Graphics();

    constructor(public x: number, public y: number, public width = 1, public height = 1) {
    }

    public registerGraphics(container: PIXI.Container): void {
        this.draw();
        container.addChild(this.graphics);
    }

    public draw(): void {
        this.graphics.beginFill(0x000);
        this.graphics.drawRect(this.x, this.y, this.width, this.height);
        this.graphics.endFill();
    }
}
