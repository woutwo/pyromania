import {Cell} from "../grid/cell";

export interface EntityInterface {
    cell: Cell;
    x: number;
    y: number;
    width: number;
    height: number;
    graphics: PIXI.Graphics;

    /**
     * (re)draw the graphics
     */
    draw(): void;

    /**
     * Register the graphics to the given container
     * @param container
     */
    registerGraphics(container: PIXI.Container): void;
}
