/**
 * Calculate temperature from given percentage
 * @param percentage
 * @constructor
 */
export function ColorFromPercentage(percentage: number) {
    let r;
    let g;
    const b = 0;
    if (percentage < 50) {
        r = 255;
        g = Math.round(5.1 * percentage);
    } else {
        g = 255;
        r = Math.round(510 - 5.10 * percentage);
    }
    return r * 0x10000 + g * 0x100 + b;
}
