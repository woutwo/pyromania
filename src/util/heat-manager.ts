import {Game} from "../game";
import {Cell} from "../grid/cell";

export class HeatManager {

    protected heatChanges: { [index: number]: number } = {};

    constructor(protected game: Game) {

    }

    /**
     * Spread heat between cells
     */
    public spreadHeat() {
        const timeStart = performance.now();
        this.resetState();

        // Calculate heat spreading
        for (const cell of this.game.grid.cells.filter(c => c.energy > 0)) {
            this.spreadToNeighbours(cell);
        }

        // Process heat changes
        for (const index of Object.keys(this.heatChanges)) {
            this.game.grid.cells[+index].energy += this.heatChanges[+index];
        }

        console.log("Heat spread took %d.3 ms", performance.now() - timeStart);
    }

    /**
     * Reset manager state
     */
    protected resetState() {
        this.heatChanges = {};
    }

    /**
     * Register heat change for cell
     * @param cell
     * @param change
     */
    protected registerHeatChange(cell: Cell, change: number) {
        const index = this.game.grid.getCellIndex(cell.x, cell.y);
        this.heatChanges[index] = (this.heatChanges[index] || 0) + change;
    }

    /**
     * Sprad heat to neighbouring cells
     * @param cell
     */
    private spreadToNeighbours(cell: Cell) {
        const receivingNeighbours: Array<[Cell, number]> = [];

        // Find direct neighbours eligible to receive energy from current cell
        for (const coord of [[-1, 0], [1, 0], [0, -1], [0, 1]]) {
            const neighbour = this.game.grid.getCellAt(cell.x + coord[0], cell.y + coord[1]);
        }

        // Distribute heat to neighbouring cells
        const totalReceivingHeat = receivingNeighbours.reduce((c, v) => c + v[1], 0);
        if (totalReceivingHeat === 0) {
            return;
        }

        // Distribute heat based on heat diff share
        for (const rn of receivingNeighbours) {
            const cellEnergy = rn[1] * (rn[1] / totalReceivingHeat);
            this.registerHeatChange(rn[0], cellEnergy);
            // Lower providing cell energy
            this.registerHeatChange(cell, -cellEnergy);
        }
    }
}
