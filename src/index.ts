import {Wall} from "./entity/wall";
import {Game} from "./game";
import {Crate} from "./entity/crate";

export let game: Game;
export let screenW: number;
export let screenH: number;

document.addEventListener("DOMContentLoaded", () => {

    // Calculate screen size
    screenW = window.innerWidth;
    screenH = window.innerHeight;

    // Construct new game for given pixi app
    game = new Game();
    game.init();

    // Add tiles
    // const tiles: Array<[number, number]> = [
    //     [-2, -2], [-1, -2], [0, -2], [1, -2], [2, -2],
    //     [-2, -1], /*                       */ [2, -1],
    //     [-2, -0], /*                       */
    //     [-2, +1], /*                       */ [2, +1],
    //     [-2, +2], [-1, +2], [0, +2], [1, +2], [2, +2],
    // ];
    // for (const tile of tiles) {
    //     game.addEntity(new Wall(...tile));
    // }

    // Incinerate center cell
    game.addEntity(new Crate(0, 0));
    // game.grid.getCellAt(0, 0).energy = 1000;

    document.addEventListener("keydown", (event: KeyboardEvent) => {
        if (event.key === " ") {
            game.heatManager.spreadHeat();
        }
    });
});
