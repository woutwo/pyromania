# Pyromania game

## How to install
To install the development project, `NodeJS` (10.7.0+ preferred) is required on your machine.

Installation:

1. Install dependencies with `npm install`
2. Run `npm run serve` to run the development server
3. Open `http://localhost:3000` in your browser to view the live development server